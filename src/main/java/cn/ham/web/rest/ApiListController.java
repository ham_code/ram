package cn.ham.web.rest;

import cn.ham.entity.Project;
import cn.ham.entity.RestApi;
import cn.ham.service.ProjectService;
import cn.ham.service.RestApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/24 21:31
 */
@Controller
@RequestMapping("api")
public class ApiListController {

    @Autowired
    private RestApiService restApiService;
    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "list/{id}",method = RequestMethod.GET)
    public String list(ModelMap modelMap,@PathVariable("id") Integer id){
        List<RestApi> restApis = restApiService.getListByProId(id);
        Project project = projectService.getById(id);
        modelMap.addAttribute("restApis",restApis);
        modelMap.addAttribute("project",project);
        return "api/list";
    }

    @RequestMapping(value = "/test/{id}",method = RequestMethod.GET)
    public String test(ModelMap modelMap,@PathVariable("id") Integer id){
        System.out.print("---");
        return "api/test";
    }
}
