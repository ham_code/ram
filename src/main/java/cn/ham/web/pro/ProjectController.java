package cn.ham.web.pro;

import cn.ham.entity.Project;
import cn.ham.service.ProjectService;
import cn.ham.utils.ResponseMsg;
import cn.ham.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 18号
 * @date 2016/11/25 20:38
 */
@Controller
@RequestMapping("/pro")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("/createPro")
    public String createPro(){
        return "/pro/createPro";
    }

    @RequestMapping("/updatePro/{id}")
    public String updatePro(ModelMap modelMap,@PathVariable("id") Integer id){
        modelMap.addAttribute("project",projectService.getById(id));
        return "/pro/updatePro";
    }

    @RequestMapping(value = "/build",method = RequestMethod.POST)
    @ResponseBody
    public Result build(Project project){
        projectService.build(project);
        return new Result(ResponseMsg.SUCCESS);
    }
}
