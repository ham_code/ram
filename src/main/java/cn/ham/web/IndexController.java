package cn.ham.web;

import cn.ham.entity.Project;
import cn.ham.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/25 16:08
 */
@Controller
public class IndexController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping("/")
    public String index(ModelMap modelMap){
        List<Project> list = projectService.getAll();
        modelMap.addAttribute("list",list);
        modelMap.addAttribute("host","welcome velocity!");
        return "index";
    }
}
