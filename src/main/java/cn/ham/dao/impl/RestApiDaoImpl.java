package cn.ham.dao.impl;

import cn.ham.dao.RestApiDao;
import cn.ham.entity.RestApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/24 20:43
 */
@Repository
public class RestApiDaoImpl implements RestApiDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(RestApi restApi) {
        jdbcTemplate.update("INSERT INTO rest_api (project_id,url,method,params) VALUES (?,?,?,?)",
                restApi.getProjectId(),restApi.getUrl(),restApi.getMethod(),restApi.getParams());
    }

    @Override
    public List<RestApi> selectAll() {
        List result = jdbcTemplate.query("SELECT id,project_id AS projectId,url,method,params FROM rest_api",
                new BeanPropertyRowMapper(RestApi.class));
        return result;
    }

    @Override
    public List<RestApi> selectByProId(int proId) {
        List result = jdbcTemplate.query("SELECT id,project_id AS projectId,url,method FROM rest_api WHERE project_id = ?",
                new BeanPropertyRowMapper(RestApi.class),proId);
        return result;
    }
}
