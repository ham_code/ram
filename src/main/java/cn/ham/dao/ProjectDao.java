package cn.ham.dao;

import cn.ham.entity.Project;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/26 19:03
 */
public interface ProjectDao {
    int save(Project project);

    List<Project> selectAll();

    Project selectById(int proId);
}
