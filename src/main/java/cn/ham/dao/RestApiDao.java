package cn.ham.dao;

import cn.ham.entity.RestApi;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/24 20:42
 */
public interface RestApiDao {
    void add(RestApi restApi);

    List<RestApi> selectAll();

    List<RestApi> selectByProId(int proId);
}
