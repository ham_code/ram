package cn.ham.file.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.List;

/**
 * 扫描指定目录下的class文件
 * @author 18号
 * @date 2016/11/23 14:33
 */
public class ScanFile {
    private static String CLASS_ROOT = "target/classes/";
    private static Logger logger = LoggerFactory.getLogger(ScanFile.class);

    /**
     * 扫描文件
     * @param classList class 列表
     * @param scanPath 扫描地址
     * @param originalPath 原始地址
     */
    public static void scan(List<String> classList,String scanPath, String originalPath){
        logger.info("scanPath is:{},originalPath is:{}",new Object[]{scanPath,originalPath});
        File rootDir = new File(scanPath);
        if(!rootDir.isDirectory()){
            String fileDir = rootDir.getAbsolutePath().toString();
            if (fileDir.endsWith(".class") && !fileDir.contains("$")){
                String clazz = fileDir.substring(originalPath.indexOf(CLASS_ROOT)+CLASS_ROOT.length(),fileDir.lastIndexOf("."));
                clazz = clazz.replace("/",".");
                classList.add(clazz);
            }
        }else{
            String[] fileList =  rootDir.list();
            for (int i = 0; i < fileList.length; i++) {
                scanPath = rootDir.getAbsolutePath()+"/"+fileList[i];
                scan(classList,scanPath,originalPath);
            }
        }
    }
}
