package cn.ham.entity;


/**
 * @author mr.ham
 * @date 2016/11/26 18:33
 */
public class Project extends BaseEntity{
    private Integer id;
    private String name;
    private String relyUrl;
    private String apiUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelyUrl() {
        return relyUrl;
    }

    public void setRelyUrl(String relyUrl) {
        this.relyUrl = relyUrl;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
}
