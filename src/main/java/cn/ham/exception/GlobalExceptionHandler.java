package cn.ham.exception;

import cn.ham.utils.BusinessException;
import cn.ham.utils.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 18号
 * @date 2016/11/16 21:48
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ModelAndView defaultErrorHandler(HttpServletRequest request,Exception ex) throws Exception{
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        mav.addObject("url", request.getRequestURL());
        mav.setViewName("error");
        return mav;
    }

    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public Result jsonErrorHandler(HttpServletRequest request, BusinessException e) throws Exception {
        Result exceptionResult = new Result(e.getCode(),e.getMessage(),null);
        return exceptionResult;
    }

}
