package cn.ham.utils;

import java.util.Arrays;
import java.util.List;

/**
 * @author 18号
 * @date 2016/11/25 9:50
 */
public class DataType {
    public final static List<String> BASIC_DATA_TYPE =
            Arrays.asList(new String[]{"java.lang.Boolean","java.lang.Byte",
                    "java.lang.Float","java.lang.Integer","java.lang.Long",
                    "java.lang.Short","java.lang.Double","java.lang.Character"});
}
