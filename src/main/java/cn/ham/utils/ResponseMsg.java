package cn.ham.utils;

/**
 * @author mr.ham
 * @date 2016/11/26 18:40
 */
public class ResponseMsg {
    public final static String SUCCESS = "200";
    public final static String ERROR = "500";
}
