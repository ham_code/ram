package cn.ham.utils;

import java.util.Date;

/**
 * @author mr.ham
 * @date 2016/11/26 18:26
 */
public class Result {
    private String code;
    private long time = new Date().getTime();
    private String msg;
    private String data;

    public Result(String code) {
        this.code = code;
    }

    public Result(String code, String data){
        this.code = code;
        this.data = data;
    }


    public Result(String code, String msg, String data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
