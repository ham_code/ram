package cn.ham.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author 18号
 * @date 2016/11/28 10:08
 */
public class ShellUtil {
    private static Logger logger = LoggerFactory.getLogger(ShellUtil.class);

    /**
     * 执行shell命令
     * @param shell
     */
    public static void shellTask(String shell){
        Runtime runtime = Runtime.getRuntime();
        BufferedReader br = null;
        try {
            Process process = runtime.exec(new String[] {"/bin/sh","-c",shell});
            br = new BufferedReader(new InputStreamReader(process.getInputStream(), "utf-8"));
            String shellLog;
            while ((shellLog = br.readLine()) != null) {
                logger.info(shellLog);
            }
        } catch (IOException e) {
            logger.error("shellTask error:{}",e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                logger.error("BufferedReader close error:{}",e);
            }
        }
    }
}