package cn.ham.utils;

import org.apache.commons.lang.StringUtils;

/**
 * @author 18号
 * @date 2016/11/26 18:37
 */
public class BusinessException extends RuntimeException {
	private String code;
	private static final long serialVersionUID = -6811768910836758978L;
	public BusinessException(String message) {
		super(message);
		this.code=message;
	}
	public BusinessException(String message,String code){
		this(StringUtils.isBlank(message)?code:message);
		this.code=code;
	}
	public String getCode() {
		return code;
	}
	public BusinessException setCode(String code) {
		this.code = code;
		return this;
	}
}

