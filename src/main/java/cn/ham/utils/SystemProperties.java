package cn.ham.utils;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 18号
 * @date 2016/11/28 14:42
 */
@ConfigurationProperties(prefix = "system")
public class SystemProperties {

    private String classroot;
    private String prodir;

    public String getClassroot() {
        return classroot;
    }

    public void setClassroot(String classroot) {
        this.classroot = classroot;
    }

    public String getProdir() {
        return prodir;
    }

    public void setProdir(String prodir) {
        this.prodir = prodir;
    }
}
