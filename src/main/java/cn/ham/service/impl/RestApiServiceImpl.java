package cn.ham.service.impl;

import cn.ham.dao.RestApiDao;
import cn.ham.entity.RestApi;
import cn.ham.service.RestApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/24 20:40
 */
@Service
public class RestApiServiceImpl implements RestApiService{

    private static Logger logger = LoggerFactory.getLogger(RestApiService.class);
    @Autowired
    private RestApiDao restApiDao;

    @Override
    public void add(List<RestApi> list) {
        for (RestApi restApi:list) {
            restApiDao.add(restApi);
        }
    }

    @Override
    public List<RestApi> getAll() {
        return restApiDao.selectAll();
    }

    @Override
    public List<RestApi> getListByProId(int proId) {
        return restApiDao.selectByProId(proId);
    }
}
