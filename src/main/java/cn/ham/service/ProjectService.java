package cn.ham.service;

import cn.ham.entity.Project;

import java.util.List;

/**
 * @author mr.ham
 * @date 2016/11/26 18:37
 */
public interface ProjectService {
    void build(Project project);

    List<Project> getAll();

    Project getById(int proId);
}
