package cn.ham.service;

import cn.ham.entity.RestApi;

import java.util.List;

/**
 * @author 18号
 * @date 2016/11/24 20:40
 */
public interface RestApiService {
    void add(List<RestApi> list);
    List<RestApi> getAll();

    List<RestApi> getListByProId(int proId);
}
